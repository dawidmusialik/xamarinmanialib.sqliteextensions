#  XamarinManiaLib.SQLiteExtensions

Przygotował Dawid Musialik.

Celem stworzenia tej aplikacji było napisanie biblioteki, która będzie wykorzystywana w aplikacjach mojego autorstwa. Ma ona za zadanie ułatwić pracę z bazą danych SQLite.Net (https://github.com/praeclarum/sqlite-net).

### Instalacja
Należy dodać odwołanie do projektu “XamarinManiaLib.SQLiteExtensions”. Oraz dyrektywy w klasach, które będą używały biblioteki: 

     using XamarinManiaLib.SQLiteExtensions;

### INFORMACJA
Projekt nie został ukończony, jest to pierwszy etap prac. 
Obecnie trwają testy nad następującymi funkcjonalnościami:
* Pobrać nazwy kolumn z tabeli w kodzie
* Sprawdzić czy istnieją różnicę między tabelą w pliku bazy danych a tabelą w kodzie
* Pobrać listę różnic różnic między tabelą w pliku bazy danych a tabelą w kodzie
* Dodać ręcznie kolumnę z kodu do istniejącej tabeli w bazie danych
* Dodać kolumnę z kodu do istniejącej tabeli w bazie danych na podstawie tabeli z kodu
* Usunąć ręcznie kolumnę z kodu do istniejącej tabeli w bazie danych
* Usunąć kolumnę z kodu do istniejącej tabeli w bazie danych na podstawie tabeli z kodu
* Dokonać migracji
