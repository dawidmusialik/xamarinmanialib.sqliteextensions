﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using XamarinManiaLib.SQLiteExtensions.Converter;
using XamarinManiaLib.SQLiteExtensions.Model;

namespace XamarinManiaLib.SQLiteExtensions.Helper
{
    internal class DatabaseMethodHelper
    {

        internal List<SQLiteColumnModel> GetTableColumns(Type Table)
        {
            List<SQLiteColumnModel> _tmpListSQLiteColumn = new List<SQLiteColumnModel>();

            if (Table.GetProperties().Select(x => x.Name).Count<string>() != 0)
            {
                foreach (var item in Table.GetProperties())
                {
                    ObservableCollection<Type> _tmpListAttribute = new ObservableCollection<Type>();
                    foreach (var attrr in item.CustomAttributes)
                    {
                        _tmpListAttribute.Add(attrr.AttributeType);
                    }

                    if (_tmpListAttribute.Contains(typeof(SQLite.ColumnAttribute)))
                    {
                        _tmpListAttribute.Remove(typeof(SQLite.ColumnAttribute));
                    }

                    _tmpListSQLiteColumn.Add(new SQLiteColumnModel(item.Name, SQLiteTypeConverter.GetType(Type.GetType(item.PropertyType.FullName)), _tmpListAttribute.ToList()));
                }
            }
            else
            {
                throw new Exception("Tabela " + Table.Name + " nie posiada properties.");
            }

            return _tmpListSQLiteColumn;
        }

        ///Sprawdzenie czy tabela zawiera atrybut tabeli SQLite
        #region CheckTableHasSQLTableAttribute
        /// <summary>
        /// Sprawdzenie czy przekazana tabela zawiera atrybut informujący że jest tabelą SQLite
        /// </summary>
        /// <param name="Table">Nazwa tabeli</param>
        /// <returns>Informacja czy operacja się powiodła.</returns>
        internal bool CheckTableHasSQLTableAttribute(Type Table)
        {
            bool _Status = false;
            string _SQLTableAttrName = typeof(SQLite.TableAttribute).Name;

            foreach (var attrr in Table.CustomAttributes)
            {
                if (attrr.AttributeType.Name.Contains(_SQLTableAttrName))
                {
                    _Status = true;
                    break;
                }
            }

            if (!_Status)
            {
                throw new Exception("Tabela " + Table.Name + "nie jest tabelą SQLite!");
            }

            return _Status;
        }
        #endregion CheckTableHasSQLTableAttribute

        ///Pobranie nazw kolum z tabeli w bazie danych
        #region GetTableColumnNames
        /// <summary>
        /// Pobranie nazw kolum z tabeli w bazie danych
        /// </summary>
        /// <param name="Table">Nazwa tabeli</param>
        /// <returns>Listę string z nazwami kolumn w tabeli</returns>
        internal List<string> GetTableColumnNames(Type Table, SQLiteConnection Connection)
        {
            List<string> _tmpListString = new List<string>();
            var TableInfo = Connection.GetTableInfo(Table.Name);
            if (TableInfo.Count != 0)
            {
                foreach (var item in TableInfo)
                {
                    _tmpListString.Add(((SQLite.SQLiteConnection.ColumnInfo)item).Name);
                }
            }
            else
            {
                throw new Exception("Nie znaleziono tabeli: " + Table.Name);
            }

            return _tmpListString;
        }
        #endregion GetTableColumnNames

        /// Pobranie nazw properties z tabeli w kodzie
        #region GetTablePropertiesName
        /// <summary>
        /// Pobranie nazw properties z tabeli w kodzie
        /// </summary>
        /// <param name="Table">Nazwa tabeli</param>
        /// <returns>Listę string z properties kolumn w tabeli</returns>
        internal List<string> GetTablePropertiesName(Type Table)
        {
            List<string> _tmpListString = new List<string>();
            if (Table.GetProperties().Select(x => x.Name).Count<string>() != 0)
            {
                foreach (var item in Table.GetProperties())
                {
                    _tmpListString.Add(item.Name);
                }
            }
            else
            {
                throw new Exception("Tabela " + Table.Name + " nie posiada properties.");
            }

            return _tmpListString;
        }
        #endregion GetTablePropertiesName

        /// Pobranie nazw i typów properties z tabeli w kodzie
        #region GetTablePropertiesNameAndType
        /// <summary>
        /// Pobranie nazw i typów properties z tabeli w kodzie
        /// </summary>
        /// <param name="Table">Nazwa tabeli</param>
        /// <returns>Listę nazw i typów z properties kolumn w tabeli</returns>
        internal List<PropertiesNameAndTypeModel> GetTablePropertiesNameAndType(Type Table)
        {
            List<PropertiesNameAndTypeModel> _tmpListPropertisNameAndType = new List<PropertiesNameAndTypeModel>();
            if (Table.GetProperties().Select(x => x.Name).Count<string>() != 0)
            {
                foreach (var item in Table.GetProperties())
                {
                    _tmpListPropertisNameAndType.Add(new PropertiesNameAndTypeModel(item.Name, item.PropertyType.FullName));
                }
            }
            else
            {
                throw new Exception("Tabela " + Table.Name + " nie posiada properties.");
            }

            return _tmpListPropertisNameAndType;
        }
        #endregion GetTablePropertiesNameAndType
    }
}
