﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace XamarinManiaLib.SQLiteExtensions.Helper
{
    internal class DatabaseCommandHelper
    {
        private DatabaseMethodHelper DBExtensionsMethod = new DatabaseMethodHelper();
        internal DatabaseCommandHelper() { }

        internal string CreateTemporaryTable(Type temporaryTableType, string temporaryTableName)
        {
            var ListTableColumns = DBExtensionsMethod.GetTableColumns(temporaryTableType);
            string _tmpCommandColumns = "( ";
            foreach (var item in ListTableColumns)
            {
                _tmpCommandColumns += item.GetCreateColumnString() + ", ";
            }
            _tmpCommandColumns = _tmpCommandColumns.Remove(_tmpCommandColumns.Length - 2, 2);
            _tmpCommandColumns += " )";

            return "CREATE TABLE " + temporaryTableName + " " + _tmpCommandColumns + ";";
        }

        internal string CopyToTable(string updatedTableName, string columnToCopy, string existingTableName)
        {
            return "INSERT INTO " + updatedTableName + " (" + columnToCopy + ")" + " SELECT " + columnToCopy + " FROM " + existingTableName + ";";
        }

        internal string DropTable(string _tableToDrop)
        {
            return "DROP TABLE " + _tableToDrop + ";";
        }

        internal string RenameTable(string _oldName, string newName)
        {
            return "ALTER TABLE " + _oldName + " RENAME TO " + newName + ";";
        }

        internal string AddColumnToTable(string _tableToModify, string _newColumnName, string _newColumnSQLiteType, string _newColumnDefaultValue)
        {
            string _tmpDefaultValue = "";
            if (!string.IsNullOrWhiteSpace(_newColumnDefaultValue))
            {
                _tmpDefaultValue = " DEFAULT '" + _newColumnDefaultValue + "'";
            }

            return "ALTER TABLE " + _tableToModify + " ADD COLUMN \"" + _newColumnName + "\"  " + _newColumnSQLiteType + _tmpDefaultValue + ";";

        }

        internal bool ExecuteCommandToRebild(Type tableToModify, ObservableCollection<string> listColumnToCopy, List<string> deleteColumnName, SQLiteConnection dbConnection)
        {
            SQLite.SQLiteCommand _tmpCommand = new SQLiteCommand(dbConnection);

            bool _status = false;
            string _tmpTableName = "tmp_" + tableToModify.Name;
            string _tmpColumnToCopy = string.Empty;
            if (listColumnToCopy.Count != 0)
            {
                string _tmpString = "";
                foreach (var item in listColumnToCopy)
                {
                    _tmpString += item + ", ";
                }

                _tmpString = _tmpString.Remove(_tmpString.Length - 2);
                _tmpColumnToCopy += _tmpString;
            }
            else
            {
                _tmpColumnToCopy = "*";
            }

            int JobStatus = 1;
            try
            {
                _tmpCommand.CommandText += CreateTemporaryTable(tableToModify, _tmpTableName);
                _tmpCommand.CommandText += CopyToTable(_tmpTableName, _tmpColumnToCopy, tableToModify.Name);
                _tmpCommand.CommandText += DropTable(tableToModify.Name);
                _tmpCommand.CommandText += RenameTable(_tmpTableName, tableToModify.Name);

                JobStatus = _tmpCommand.ExecuteNonQuery();
                #region
                //try
                //{
                //    _tmpCommand.CommandText = CreateTemporaryTable(tableToModify, _tmpTableName);
                //    JobStatus = _tmpCommand.ExecuteNonQuery();

                //}
                //catch (Exception exception)
                //{
                //    Logger.LogErrorMessage(LogType.Log, "Exception: " + exception.StackTrace + " " + exception.InnerException.StackTrace);
                //    throw exception;
                //}
                //try
                //{
                //    _tmpCommand.CommandText = CopyToTable(_tmpTableName, _tmpColumnToCopy, tableToModify.Name);
                //    JobStatus = _tmpCommand.ExecuteNonQuery();

                //}
                //catch (Exception exception)
                //{
                //    Logger.LogErrorMessage(LogType.Log, "Exception: " + exception.StackTrace + " " + exception.InnerException.StackTrace);
                //    throw exception;
                //}

                //try
                //{
                //    //_tmpCommand.CommandText = "DROP TABLE " + TableToModify.Name + ";";
                //    _tmpCommand.CommandText = DropTable(tableToModify.Name);

                //    JobStatus = _tmpCommand.ExecuteNonQuery();
                //}
                //catch (Exception exception)
                //{
                //    Logger.LogErrorMessage(LogType.Log, "Exception: " + exception.StackTrace + " " + exception.InnerException.StackTrace);
                //    throw exception;
                //}

                //try
                //{
                //    //_tmpCommand.CommandText = "ALTER TABLE " + _tmpTableName + " RENAME TO " + TableToModify.Name + ";";
                //    _tmpCommand.CommandText = RenameTable(_tmpTableName, tableToModify.Name);

                //    JobStatus = _tmpCommand.ExecuteNonQuery();
                //}
                //catch (Exception exception)
                //{
                //    Logger.LogErrorMessage(LogType.Log, "Exception: " + exception.StackTrace + " " + exception.InnerException.StackTrace);
                //    throw exception;
                //}
                #endregion
            }
            catch (Exception exception)
            {
                throw exception;
            }

            if (JobStatus == 0)
            {
                _status = true;

                string _tmpListDeletedColumn = "";
                foreach (var item in deleteColumnName)
                {
                    _tmpListDeletedColumn += item;
                }

            }
            else _status = false;

            return _status;
        }
    }
}