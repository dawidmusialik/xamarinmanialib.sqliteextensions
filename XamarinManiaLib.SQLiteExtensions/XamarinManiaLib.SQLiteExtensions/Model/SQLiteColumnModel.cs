﻿using System;
using System.Collections.Generic;
using System.Text;
using XamarinManiaLib.SQLiteExtensions.Converter;
using XamarinManiaLib.SQLiteExtensions.Model.Enum;

namespace XamarinManiaLib.SQLiteExtensions.Model
{
    internal class SQLiteColumnModel
    {
        private string Name { get; set; }
        private string Type { get; set; }
        private List<Type> ListAttribute { get; set; }
        private string Attributes { get; set; }

        internal SQLiteColumnModel(string name, SQLiteTypeEnum type, List<Type> listAttribute)
        {
            ListAttribute = new List<Type>();
            ListAttribute = listAttribute;
            Name = name;
            Type = SQLiteTypeConverter.GetTypeText(type);

            if (ListAttribute.Count != 0)
            {
                foreach (var item in ListAttribute)
                {
                    Attributes += SQLiteAttributeConverter.GetSQLiteAttributeText(item) + " ";
                }
                Attributes = Attributes.Remove(Attributes.Length - 1, 1);
            }
        }
        internal string GetCreateColumnString()
        {
            return Name + " " + Type + " " + Attributes;
        }
    }
}
