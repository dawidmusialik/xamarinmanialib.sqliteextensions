﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.SQLiteExtensions.Model.Enum
{
    internal enum SQLiteAttributeEnum
    {
        UNIQUE = 0,
        PRIMARYKEY = 1,
        NOTNULL = 2,
        AUTOINCREMENT = 3,
        Error = 999
    }
}
