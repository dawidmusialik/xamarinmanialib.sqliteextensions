﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.SQLiteExtensions.Model.Enum
{
    public enum SQLiteTypeEnum
    {
        INT = 0,
        LONG = 1,
        BIGINT = 2,
        STRING = 3,
        BOOL = 4,
        DOUBLE = 5,
        FLOAT = 6,
        DECIMAL = 7,
        CHAR = 8,
        BYTE = 9,
        SBYTE = 10,
        UINT = 11,
        ULONG = 12,
        SHORT = 13,
        USHORT = 14
    }
}
