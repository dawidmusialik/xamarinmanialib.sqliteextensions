﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.SQLiteExtensions.Model
{
    internal class PropertiesNameAndTypeModel
    {
        public PropertiesNameAndTypeModel() { }
        public PropertiesNameAndTypeModel(string _Name, string _Type) { Name = _Name; Type = _Type; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
