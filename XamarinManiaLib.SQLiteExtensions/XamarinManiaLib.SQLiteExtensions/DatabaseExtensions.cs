﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using XamarinManiaLib.SQLiteExtensions.Converter;
using XamarinManiaLib.SQLiteExtensions.Helper;
using XamarinManiaLib.SQLiteExtensions.Model;
using XamarinManiaLib.SQLiteExtensions.Model.Enum;

namespace XamarinManiaLib.SQLiteExtensions
{
    public class DatabaseExtensions
    {
        private SQLiteConnection _DatabaseConnection;
        private DatabaseCommandHelper DBExtensionsCommand = new DatabaseCommandHelper();
        private DatabaseMethodHelper DBExtensionsMethod = new DatabaseMethodHelper();

        public DatabaseExtensions() { }
        public DatabaseExtensions(SQLiteConnection dbConnection)
        {
            _DatabaseConnection = dbConnection;
        }

        ///Pobranie nazw kolumn z tabeli bazy danych
        #region GetTableColumnName
        /// <summary>
        /// Pobranie kolumn w danej tabeli bazy danych
        /// </summary>
        /// <param name="Table">Klasa reprezentująca tabelę w bazie danych</param>
        /// <returns>Listę kolumn, które są w bazie danych na podstawie nazwy przekazanej tabeli</returns>
        public List<string> GetTableColumnName(Type Table)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(Table);

               // DatabaseConnectionOpen();

                return DBExtensionsMethod.GetTableColumnNames(Table, _DatabaseConnection);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }
        #endregion GetTableColumnName

        /// Sprawdzenie czy istnieje różnica w tabelach na bazie danych a kodzie
        #region IsDifferentBetweenTable
        /// <summary>
        /// Sprawdzenie czy istnieje różnia w kolumnach tabeli na bazie danych, a tabeli zdefiniowanej w kodzie
        /// </summary>
        /// <param name="Table">Klasa reprezentująca tabelę</param>
        /// <returns>Informacja czy istnieją różnicę pomiędzy tabelami</returns>
        public bool IsDifferentBetweenTable(Type TableName)
        {
            return _IsDifferentBetweenTable(TableName, TableName);
        }
        /// <summary>
        /// Sprawdzenie czy istnieje różnia w kolumnach tabeli na bazie danych, a tabeli zdefiniowanej w kodzie
        /// </summary>
        /// <param name="_OldTableName">Klasa reprezentująca tabelę w bazie danych</param>
        /// <param name="_NewTableName">Klasa reprezentująca tabelę w kodzie</param>
        /// <returns>Informacja czy istnieją różnicę pomiędzy tabelami</returns>
        public bool IsDifferentBetweenTable(Type _OldTableName, Type _NewTableName)
        {
            return _IsDifferentBetweenTable(_OldTableName, _NewTableName);
        }
        private bool _IsDifferentBetweenTable(Type _OldTableName, Type _NewTableName)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_OldTableName);
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_NewTableName);

                //DatabaseConnectionOpen();
                bool _Status = false;

                List<string> _tmpListColumnTable = new List<string>();

                _tmpListColumnTable = DBExtensionsMethod.GetTableColumnNames(_OldTableName, _DatabaseConnection);

                List<string> _tmpListPropertiesNewTable = new List<string>();

                _tmpListPropertiesNewTable = DBExtensionsMethod.GetTablePropertiesName(_NewTableName);

                foreach (var item in _tmpListPropertiesNewTable)
                {
                    if (!_tmpListColumnTable.Contains(item))
                    {
                        _Status = true;
                        break;
                    }
                }

                return _Status;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }
        #endregion

        /// Sprawdzenie czy istnieje różnica w tabelach na bazie danych a kodzie
        #region DiferentBetweenTwoTable
        /// <summary>
        /// Sprawdzenie czy istnieje różnia w kolumnach tabeli na bazie danych, a tabeli zdefiniowanej w kodzie. I zwrócenie listy różnic
        /// </summary>
        /// <param name="_TableName">Klasa reprezentująca tabelę</param>
        /// <returns>Lista różnic względem tabel</returns>
        public List<string> DiferentBetweenTable(Type _TableName)
        {
            return _DiferentBetweenTable(_TableName, _TableName);
        }
        /// <summary>
        /// Sprawdzenie czy istnieje różnia w kolumnach tabeli na bazie danych, a tabeli zdefiniowanej w kodzie. I zwrócenie listy różnic
        /// </summary>
        /// <param name="_OldTableName">Klasa reprezentująca tabelę w bazie danych</param>
        /// <param name="_NewTableName">Klasa reprezentująca tabelę w kodzie</param>
        /// <returns></returns>
        public List<string> DiferentBetweenTable(Type _OldTableName, Type _NewTableName)
        {
            return _DiferentBetweenTable(_OldTableName, _NewTableName);
        }

        /// <summary>
        /// Sprawdzenie czy istnieje różnica w kolumnach tabeli na bazie danych, a tabeli zdefiniowanej w kodzie
        /// </summary>
        /// <param name="_OldTableName">Klasa reprezentująca tabelę w bazie danych</param>
        /// <param name="_NewTableName">Klasa reprezentująca tabelę w kodzie</param>
        /// <returns>Lista różnic</returns>
        private List<string> _DiferentBetweenTable(Type _OldTableName, Type _NewTableName)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_OldTableName);
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_NewTableName);

                //DatabaseConnectionOpen();

                List<string> _tmpListColumnTable = new List<string>();
                _tmpListColumnTable = DBExtensionsMethod.GetTableColumnNames(_OldTableName, _DatabaseConnection);

                List<string> _tmpListPropertiesNewTable = new List<string>();
                _tmpListPropertiesNewTable = DBExtensionsMethod.GetTablePropertiesName(_NewTableName);

                List<string> _tmpListStringDifrent = new List<string>();

                foreach (var item in _tmpListPropertiesNewTable)
                {
                    if (!_tmpListColumnTable.Contains(item))
                    {
                        _tmpListStringDifrent.Add(item);
                    }
                }


                if (_tmpListStringDifrent.Count == 0) return new List<string>();
                else return _tmpListStringDifrent;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }
        #endregion DiferentBetweenTwoTable

        /// Dodanie kolumn do tabeli na bazie danych.
        #region InsertColumnInTable
        /// <summary>
        /// Dodanie kolumn z nowej tabeli w kodzie do już istniejącej na bazie danych.
        /// </summary>
        /// <param name="_OldTableName">Klasa reprezentująca tabelę w bazie danych </param>
        /// <param name="_NewTableName">Klasa reprezentująca tabelę w kodzie </param>
        /// <returns>Informacje czy operacja się powiodła</returns>
        public bool InsertColumnInTable(Type _OldTableName, Type _NewTableName)
        {
            return _InsertColumnInTable(_OldTableName, _NewTableName);
        }
        /// <summary>
        /// Dodanie kolumn z nowej tabeli w kodzie do już istniejącej na bazie danych.
        /// </summary>
        /// <param name="_TableName">Klasa reprezentująca tabelę</param>
        /// <returns>Informacje czy operacja się powiodła</returns>
        public bool InsertColumnInTable(Type _TableName)
        {
            return _InsertColumnInTable(_TableName, _TableName);
        }
        /// <summary>
        /// Dodanie nowej kolumny do istniejącej tabelii
        /// </summary>
        /// <param name="TableToModify">Klasa reprezentująca tabelę w bazie danych do modyfikacji</param>
        /// <param name="_NewColumnName">Nazwa dodawanej kolumny</param>
        /// <param name="_NewColumnType">Typ dodawanej kolumny</param>
        /// <param name="_NewColumnDefaultValue">Domyślna wartośc dodwanej kolumny, jeżeli pozostawiony pusty string, nie doda niczego</param>
        /// <returns></returns>
        public bool InsertColumnInTable(Type TableToModify, string _NewColumnName, SQLiteTypeEnum _NewColumnType, string _NewColumnDefaultValue = "")
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(TableToModify);

                bool _Status = false;
                //DatabaseConnectionOpen();

                List<string> _tmpListColumnTable = new List<string>();
                _tmpListColumnTable = DBExtensionsMethod.GetTableColumnNames(TableToModify, _DatabaseConnection);

                bool NewColumnIsInTable = false;
                foreach (var item in _tmpListColumnTable)
                {
                    if (item.Contains(_NewColumnName))
                    {
                        NewColumnIsInTable = true;
                    }
                }

                if (!NewColumnIsInTable)
                {
                    SQLite.SQLiteCommand _tmpCommand = new SQLiteCommand(_DatabaseConnection);

                    _tmpCommand.CommandText = DBExtensionsCommand.AddColumnToTable(TableToModify.Name, _NewColumnName, SQLiteTypeConverter.GetTypeText(_NewColumnType), _NewColumnDefaultValue);

                    int JobStatus = 1;
                    try
                    {
                        JobStatus = _tmpCommand.ExecuteNonQuery();
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }

                    if (JobStatus == 0) _Status = true;
                    else _Status = false;
                }

                return _Status;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }

        /// <summary>
        /// Dodanie kolumn z tabeli w kodzie do już istniejącej na bazie danych.
        /// </summary>
        /// <param name="_OldTableName">Klasa reprezentująca tabelę w bazie danych </param>
        /// <param name="_NewTableName">Klasa reprezentująca tabelę w kodzie </param>
        /// <returns>Informacje czy operacja się powiodła</returns>
        private bool _InsertColumnInTable(Type _OldTableName, Type _NewTableName)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_OldTableName);
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_NewTableName);

                //DatabaseConnectionOpen();

                bool Status = false;

                List<string> _tmpListColumnTable = new List<string>();
                _tmpListColumnTable = DBExtensionsMethod.GetTableColumnNames(_OldTableName, _DatabaseConnection);

                List<PropertiesNameAndTypeModel> _tmpListPropertiesNewTable = new List<PropertiesNameAndTypeModel>();
                _tmpListPropertiesNewTable = DBExtensionsMethod.GetTablePropertiesNameAndType(_NewTableName);

                ObservableCollection<PropertiesNameAndTypeModel> _tmpListStringDifrent = new ObservableCollection<PropertiesNameAndTypeModel>();

                foreach (var item in _tmpListPropertiesNewTable)
                {
                    if (!_tmpListColumnTable.Contains(item.Name))
                    {
                        _tmpListStringDifrent.Add(new PropertiesNameAndTypeModel(item.Name, item.Type));
                    }
                }

                if (_tmpListStringDifrent.Count != 0)
                {
                    ObservableCollection<PropertiesNameAndTypeModel> _tmpListStringDifrentCopy = new ObservableCollection<PropertiesNameAndTypeModel>(_tmpListStringDifrent);

                    foreach (var item in _tmpListStringDifrent)
                    {
                        if (!InsertColumnInTable(_OldTableName, item.Name, SQLiteTypeConverter.GetType(Type.GetType(item.Type)), ""))
                        {
                            Status = false;
                            break;
                        }
                        else
                        {
                            _tmpListStringDifrentCopy.Remove(item);
                        }
                    }

                    if (_tmpListStringDifrentCopy != null && _tmpListStringDifrentCopy.Count == 0) Status = true;
                    else Status = false;
                }
                else Status = false;

                return Status;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }
        #endregion InsertColumnInTable

        /// Usunięcie kolumn w tabeli na bazie danych
        #region DeleteColumnInTable
        /// <summary>
        /// Usunięcie kolumn w tabeli na bazie, zgodnie z kolumnami w kodzie
        /// </summary>
        /// <param name="_TableName">Nazwa tabeli</param>
        /// <returns> Informacja czy operacja się powiodła</returns>
        public bool DeleteColumnInTable(Type _TableName)
        {
            return DeleteColumnInTable(_TableName, _TableName);
        }

        /// <summary>
        ///  Usunięcie kolumn w tabeli na bazie, zgodnie z podanymi nazwami kolumn
        /// </summary>
        /// <param name="TableToModify">Nazwa tabeli</param>
        /// <param name="_DeleteColumnName">Lista kolumn do usunięcia</param>
        /// <returns> Informacja czy operacja się powiodła</returns>
        public bool DeleteColumnInTable(Type TableToModify, List<string> _DeleteColumnName)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(TableToModify);

                bool _Status = false;
                //DatabaseConnectionOpen();

                List<string> _tmpListColumnTable = new List<string>();
                _tmpListColumnTable = DBExtensionsMethod.GetTableColumnNames(TableToModify, _DatabaseConnection);

                bool DeleteColumnIsInTable = false;
                foreach (var item in _tmpListColumnTable)
                {
                    if (_DeleteColumnName.Contains(item))
                    {
                        DeleteColumnIsInTable = true;
                    }

                }

                if (DeleteColumnIsInTable)
                {

                    ObservableCollection<string> _tmpListColumnToCopy = new ObservableCollection<string>(_tmpListColumnTable);

                    foreach (var item in _DeleteColumnName)
                    {
                        _tmpListColumnToCopy.Remove(item);
                    }


                    _Status = DBExtensionsCommand.ExecuteCommandToRebild(TableToModify, _tmpListColumnToCopy, _DeleteColumnName, _DatabaseConnection);

                    return _Status;
                }
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }

        /// <summary>
        /// Usunięcie kolumn w tabeli na bazie, zgodnie z kolumnami w kodzie
        /// </summary>
        /// <param name="_TableName">Nazwa tabeli w bazie danych</param>
        /// <param name="_CodeTable">Nazwa tabeli w kodzie</param>
        /// <returns> Informacja czy operacja się powiodła</returns>
        public bool DeleteColumnInTable(Type _DBTable, Type _CodeTable)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_DBTable);
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_CodeTable);

                //DatabaseConnectionOpen();

                bool Status = false;

                List<string> _tmpListColumnTable = new List<string>();
                _tmpListColumnTable = DBExtensionsMethod.GetTableColumnNames(_DBTable, _DatabaseConnection);

                List<string> _tmpListPropertiesNewTable = new List<string>();
                _tmpListPropertiesNewTable = DBExtensionsMethod.GetTablePropertiesName(_CodeTable);

                List<string> _tmpListStringCodeTable = new List<string>();
                foreach (var item in _tmpListPropertiesNewTable)
                {
                    _tmpListStringCodeTable.Add(item);
                }

                List<string> _tmpListStringToDelete = new List<string>();
                foreach (var item in _tmpListColumnTable)
                {
                    if (!_tmpListStringCodeTable.Contains(item))
                    {
                        _tmpListStringToDelete.Add(item);
                    }
                }

                Status = DeleteColumnInTable(_DBTable, _tmpListStringToDelete);

                return Status;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }
        #endregion DeleteColumnInTable

        /// Przebudowanie tabeli w bazie danych 
        #region MigrationTable
        /// <summary>
        /// Przebudowanie tabeli w bazie danych względem tabeli w kodzie
        /// </summary>
        /// <param name="_dbTable">Nazwa tabeli w bazie danych</param>
        /// <param name="_codeTable">Mazwa tabeli w kodzie</param>
        /// <returns>Informacja czy operacja się powiodła</returns>
        public bool MigrationTable(Type _dbTable, Type _codeTable)
        {
            return _MigrationTable(_dbTable, _codeTable);
        }
        /// <summary>
        /// Przebudowanie tabeli w bazie danych względem tabeli w kodzie
        /// </summary>
        /// <param name="Table">Nazwa tabeli</param>
        /// <returns>Informacja czy operacja się powiodła</returns>
        public bool MigrationTable(Type Table)
        {
            return _MigrationTable(Table, Table);
        }

        /// <summary>
        /// Przebudowanie tabeli w bazie danych względem tabeli w kodzie
        /// </summary>
        /// <param name="_dbTable">Nazwa tabeli w bazie danych</param>
        /// <param name="_codeTable">Mazwa tabeli w kodzie</param>
        /// <returns>Informacja czy operacja się powiodła</returns>
        private bool _MigrationTable(Type _dbTable, Type _codeTable)
        {
            try
            {
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_dbTable);
                DBExtensionsMethod.CheckTableHasSQLTableAttribute(_codeTable);

                ObservableCollection<string> _tmpListStringDBTable = new ObservableCollection<string>();
                ObservableCollection<PropertiesNameAndTypeModel> _tmpListPropertisNameAndTypeCodeTable = new ObservableCollection<PropertiesNameAndTypeModel>();

                ObservableCollection<string> _tmpListColumnToDelete = new ObservableCollection<string>();

                ObservableCollection<PropertiesNameAndTypeModel> _tmpListColumnToInsert = new ObservableCollection<PropertiesNameAndTypeModel>();


                bool _Status = false;
                //DatabaseConnectionOpen();


                //Pobranie listy kolumn istniejących na bazie
                _tmpListStringDBTable = new ObservableCollection<string>(DBExtensionsMethod.GetTableColumnNames(_dbTable, _DatabaseConnection));


                //Pobranie listy kolumn z typem istniejących w kodzie
                _tmpListPropertisNameAndTypeCodeTable = new ObservableCollection<PropertiesNameAndTypeModel>(DBExtensionsMethod.GetTablePropertiesNameAndType(_codeTable));

                //Lista kolumn do skasowania
                foreach (var item in _tmpListStringDBTable)
                {
                    if (!_tmpListPropertisNameAndTypeCodeTable.Select(x => x.Name).Contains(item))
                    {
                        _tmpListColumnToDelete.Add(item);
                    }
                }

                //Lista kolumn do dodania
                foreach (var item in _codeTable.GetProperties())
                {
                    if (!_tmpListStringDBTable.Contains(item.Name))
                    {
                        _tmpListColumnToInsert.Add(new PropertiesNameAndTypeModel(item.Name, item.PropertyType.FullName));
                    }
                }

                //Usuwanie kolumn
                if (_tmpListColumnToDelete.Count != 0)
                {
                    _Status = DeleteColumnInTable(_dbTable, _tmpListColumnToDelete.ToList());
                    if (!_Status)
                    {
                        return false;
                    }
                }

                //Dodawanie kolumn
                if (_tmpListColumnToInsert.Count != 0)
                {
                    foreach (var item in _tmpListColumnToInsert)
                    {
                        _Status = InsertColumnInTable(_dbTable, item.Name, SQLiteTypeConverter.GetType(Type.GetType(item.Type)), "");
                        if (!_Status)
                        {
                            return false;
                        }
                    }
                }


                return _Status;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DatabaseConnectionClose();
            }
        }
        #endregion RebildTable

        /// <summary>
        /// Otwarcie połączenia z bazą danych
        /// </summary>
        public void DatabaseConnectionOpen(SQLiteConnection dbConnection)
        {
            _DatabaseConnection = dbConnection;
        }

        /// <summary>
        /// Zamknięcie połączenia z bazą danych
        /// </summary>
        public void DatabaseConnectionClose()
        {
            _DatabaseConnection.Close();
            _DatabaseConnection.Dispose();
        }
    }
}

