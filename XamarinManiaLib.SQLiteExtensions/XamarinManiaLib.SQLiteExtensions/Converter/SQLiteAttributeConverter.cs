﻿using System;
using System.Collections.Generic;
using System.Text;
using XamarinManiaLib.SQLiteExtensions.Model.Enum;

namespace XamarinManiaLib.SQLiteExtensions.Converter
{
    internal static class SQLiteAttributeConverter
    {
        internal static string GetSQLiteAttributeText(SQLiteAttributeEnum _SQLiteAttribute)
        {
            switch (_SQLiteAttribute)
            {
                case SQLiteAttributeEnum.AUTOINCREMENT: return "AUTOINCREMENT";
                case SQLiteAttributeEnum.NOTNULL: return "NOT NULL";
                case SQLiteAttributeEnum.PRIMARYKEY: return "PRIMARY KEY";
                case SQLiteAttributeEnum.UNIQUE: return "UNIQUE";
                default: return "";
            }
        }
        internal static SQLiteAttributeEnum GetSQLiteAttribute(Type _SQLiteTypeAttribute)
        {
            if (_SQLiteTypeAttribute.Name == typeof(SQLite.UniqueAttribute).Name) return SQLiteAttributeEnum.UNIQUE;
            else if (_SQLiteTypeAttribute.Name == typeof(SQLite.PrimaryKeyAttribute).Name) return SQLiteAttributeEnum.PRIMARYKEY;
            else if (_SQLiteTypeAttribute.Name == typeof(SQLite.NotNullAttribute).Name) return SQLiteAttributeEnum.NOTNULL;
            else if (_SQLiteTypeAttribute.Name == typeof(SQLite.AutoIncrementAttribute).Name) return SQLiteAttributeEnum.AUTOINCREMENT;
            else return SQLiteAttributeEnum.Error;
        }

        internal static string GetSQLiteAttributeText(Type _SQLiteTypeAttribute)
        {
            if (_SQLiteTypeAttribute.Name == typeof(SQLite.UniqueAttribute).Name) return GetSQLiteAttributeText(SQLiteAttributeEnum.UNIQUE);
            else if (_SQLiteTypeAttribute.Name == typeof(SQLite.PrimaryKeyAttribute).Name) return GetSQLiteAttributeText(SQLiteAttributeEnum.PRIMARYKEY);
            else if (_SQLiteTypeAttribute.Name == typeof(SQLite.NotNullAttribute).Name) return GetSQLiteAttributeText(SQLiteAttributeEnum.NOTNULL);
            else if (_SQLiteTypeAttribute.Name == typeof(SQLite.AutoIncrementAttribute).Name) return GetSQLiteAttributeText(SQLiteAttributeEnum.AUTOINCREMENT);
            else return GetSQLiteAttributeText(SQLiteAttributeEnum.Error);
        }

    }
}
