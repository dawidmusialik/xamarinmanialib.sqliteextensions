﻿using System;
using System.Collections.Generic;
using System.Text;
using XamarinManiaLib.SQLiteExtensions.Model.Enum;

namespace XamarinManiaLib.SQLiteExtensions.Converter
{
    internal static class SQLiteTypeConverter
    {
        internal static string GetTypeText(SQLiteTypeEnum _SQLiteType)
        {
            switch (_SQLiteType)
            {
                case SQLiteTypeEnum.BOOL: return "BOOL";
                case SQLiteTypeEnum.DECIMAL: return "NUMERIC";
                case SQLiteTypeEnum.DOUBLE: return "REAL";
                case SQLiteTypeEnum.FLOAT: return "REAL";
                case SQLiteTypeEnum.INT: return "INTEGER";
                case SQLiteTypeEnum.LONG: return "INTEGER";
                case SQLiteTypeEnum.STRING: return "TEXT";
                case SQLiteTypeEnum.CHAR: return "TEXT";
                case SQLiteTypeEnum.BYTE: return "INTEGER";
                case SQLiteTypeEnum.SBYTE: return "INTEGER";
                case SQLiteTypeEnum.UINT: return "INTEGER";
                case SQLiteTypeEnum.ULONG: return "INTEGER";
                case SQLiteTypeEnum.SHORT: return "INTEGER";
                case SQLiteTypeEnum.USHORT: return "INTEGER";

                default: return "";
            }
        }

        internal static string GetTypeText(Type NetType)
        {
            switch ((NetType.Name))
            {
                case "Boolean": return GetTypeText(SQLiteTypeEnum.BOOL);
                case "Byte": return GetTypeText(SQLiteTypeEnum.BYTE);
                case "SByte": return GetTypeText(SQLiteTypeEnum.SBYTE);
                case "Char": return GetTypeText(SQLiteTypeEnum.CHAR);
                case "Decimal": return GetTypeText(SQLiteTypeEnum.DECIMAL);
                case "Double": return GetTypeText(SQLiteTypeEnum.DOUBLE);
                case "Single": return GetTypeText(SQLiteTypeEnum.FLOAT);
                case "Int32": return GetTypeText(SQLiteTypeEnum.INT);
                case "UInt32": return GetTypeText(SQLiteTypeEnum.UINT);
                case "Int64": return GetTypeText(SQLiteTypeEnum.LONG);
                case "Int16": return GetTypeText(SQLiteTypeEnum.SHORT);
                case "UInt16": return GetTypeText(SQLiteTypeEnum.UINT);
                case "String": return GetTypeText(SQLiteTypeEnum.STRING);

                default: throw new Exception("Nie ma takiego typu");
            }

        }
        internal static SQLiteTypeEnum GetType(Type NetType)
        {
            switch ((NetType.Name))
            {
                case "Boolean": return SQLiteTypeEnum.BOOL;
                case "Byte": return SQLiteTypeEnum.BYTE;
                case "SByte": return SQLiteTypeEnum.SBYTE;
                case "Char": return SQLiteTypeEnum.CHAR;
                case "Decimal": return SQLiteTypeEnum.DECIMAL;
                case "Double": return SQLiteTypeEnum.DOUBLE;
                case "Single": return SQLiteTypeEnum.FLOAT;
                case "Int32": return SQLiteTypeEnum.INT;
                case "UInt32": return SQLiteTypeEnum.UINT;
                case "Int64": return SQLiteTypeEnum.LONG;
                case "Int16": return SQLiteTypeEnum.SHORT;
                case "UInt16": return SQLiteTypeEnum.UINT;
                case "String": return SQLiteTypeEnum.STRING;

                default: throw new Exception("Nie ma takiego typu");
            }

        }
    }
}
